<?php

namespace Nhattuanbl;
include_once __DIR__ . '/helper.php';

/**
 * Class SimpleQueue
 * Simple Async Queue with Redis
 */
class SimpleQueue
{
    /** @var string $key */
    public static $key = 'SimpleQueue:';

    /** @var \Predis\Client $client */
    protected $client;

    /**
     * @param array|string|null $parameters
     *      tls://127.0.0.1?ssl[cafile]=private.pem&ssl[verify_peer]=1
     *      unix:///path/to/redis.sock
     *      $parameters = [
     *          'scheme' => (string) Description. Default 'tcp'. Accepts 'tcp', 'unix', 'tls'
     *          'path => (string) Description. '/path/to/redis.sock'
     *          'ssl' => (array) ['cafile' => 'private.pem', 'verify_peer' => true]
     *          'host' => (string) Description. ['cafile' => 'private.pem', 'verify_peer' => true]
     *      ]
     *
     * @param array $options
     *      $options = [
     *          'cluster' => (string) 'predis',
     *          'prefix' => (string) 'SimpleQueue:',
     *      ]
     *
     * @extends \Predis\Client::__construct
     */
    public function __construct($parameters = null, $options = null)
    {
        $this->client = new \Predis\Client($parameters, $options);
        $this->database();
    }

    /** @param string|int $database */
    public function database($database = 0)
    {
        return $this->client->select($database);
    }

    public function queues()
    {
        $iterator = null;
        $items = [];

        do {
            $response = $this->client->scan($iterator, ['MATCH' => self::$key . '*']);
            $iterator = $response[0];
            $keys = $response[1];

            foreach ($keys as $key) {
                if ($this->client->type($key)->__toString() === 'list') {
                    foreach ($this->client->lrange($key, 0, -1) as $item) {
                        $items[$key] = json_decode($item, true);
                        if (json_last_error() !== JSON_ERROR_NONE) {
                            $items[$key] = $item;
                        }
                    }
                }
            }
        } while ($iterator != 0);

        return $items;
    }

    public function queue($key)
    {
        $item = $this->client->lindex($key, 0);
        $val = json_decode($item, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $val = $item;
        }

        return $val;
    }

    /**
     * @param string|array $task
     * @param string $filename
     */
    public function enQueue($task, $filename)
    {
        $k = self::$key . round(microtime(true) * 1000);
        $this->client->rpush($k, is_array($task) ? json_encode($task) : $task);
        $this->client->expire($k, 2592000);

        $phpBinaryFinder = new \Symfony\Component\Process\PhpExecutableFinder();
        $phpBinaryPath = $phpBinaryFinder->find();
        if (strncasecmp(PHP_OS, 'WIN', 3) == 0) {
            $run = "start /B $phpBinaryPath \"$filename\" > NUL";
        } else {
            $run = "$phpBinaryPath $filename > /dev/null &";
        }

        pclose(popen($run, 'r'));
    }

    public function deQueue()
    {
        $keys = $this->client->keys(self::$key . '*');
        if (empty($keys)) {
            return;
        }

        $val = $this->client->lpop($keys[0]);
        $json = json_decode($val, true);
        if (json_last_error() === JSON_ERROR_NONE) {
            $val = $json;
        }

        return $val;
    }
}
